﻿using System;
using Microsoft.Azure.NotificationHubs;

namespace notifyazure
{
    class Program
    {
        private static async void SendNotificatoinAsync(string title, string url)
        {
            var hub = NotificationHubClient.CreateClientFromConnectionString("Endpoint=sb://simplenotification.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=QPWP6ZiMAlQRVjV2a4QhqUajcByaKvVHDjEGwZ9weo4=", "SimpleNotification");
            var jsonMessage = "{ \"aps\": { \"alert\": \"" + title + "\", \"sound\": \"default\", \"link_url\" : \"" + url + "\" } } ";
            await hub.SendAppleNativeNotificationAsync(jsonMessage);
        }

        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: app.exe AlertTitle https://yufufi.com");
                return;
            }

            if (!args[1].StartsWith("https://"))
            {
                Console.WriteLine("Only https is accepted");
                return;
            }
            Program.SendNotificatoinAsync(args[0], args[1]);
            Console.ReadLine();
        }
    }
}
